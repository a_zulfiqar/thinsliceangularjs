// Module
var app = angular.module('myApp',['ui.bootstrap', 'ngResource']);

// Controller
app.controller('myController',function($scope){
      
    // Object
    $scope.persons = [
            {sno:1,name:'Abdullah Zulfiqar',gender:'Male',  editing: false},
            {sno:2,name:'Tom Campbell',gender:'Male',  editing: false},
            {sno:3,name:'John Cenn',gender:'Male',  editing: false},
            {sno:4,name:'Nathan Brackan',gender:'Male',  editing: false},
            {sno:5,name:'Abdullah Zulfiqar',gender:'Male',  editing: false},
            {sno:6,name:'Tom Campbell',gender:'Male',  editing: false},
            {sno:7,name:'Abdullah Zulfiqar',gender:'Male',  editing: false},
            {sno:8,name:'Tom Campbell',gender:'Male',  editing: false},
            {sno:9,name:'Abdullah Zulfiqar',gender:'Male',  editing: false},
            {sno:10,name:'Tom Campbell',gender:'Male',  editing: false},
            {sno:11,name:'Abdullah Zulfiqar',gender:'Male',  editing: false},
            {sno:12,name:'Tom Campbell',gender:'Male',  editing: false},
            {sno:13,name:'Abdullah Zulfiqar',gender:'Male',  editing: false},
            {sno:14,name:'Tom Campbell',gender:'Male',  editing: false},
            {sno:15,name:'Abdullah Zulfiqar',gender:'Male',  editing: false},
            {sno:16,name:'Tom Campbell',gender:'Male',  editing: false},
            {sno:17,name:'Abdullah Zulfiqar',gender:'Male',  editing: false},
            {sno:18,name:'Tom Campbell',gender:'Male',  editing: false},
            {sno:19,name:'Abdullah Zulfiqar',gender:'Male',  editing: false},
            {sno:20,name:'Tom Campbell',gender:'Male',  editing: false},
            { sno:21,name:'Abdullah Zulfiqar',gender:'Male',  editing: false},
            {sno:22,name:'Tom Campbell',gender:'Male',  editing: false},
            {sno:23,name:'John Cenn',gender:'Male',  editing: false},
            {sno:24,name:'Nathan Brackan',gender:'Male',  editing: false},
            {sno:25,name:'Abdullah Zulfiqar',gender:'Male',  editing: false},
            {sno:26,name:'Tom Campbell',gender:'Male',  editing: false},
            {sno:27,name:'Abdullah Zulfiqar',gender:'Male',  editing: false},
            {sno:28,name:'Tom Campbell',gender:'Male',  editing: false},
            {sno:29,name:'Abdullah Zulfiqar',gender:'Male',  editing: false},
            {sno:30,name:'Tom Campbell',gender:'Male',  editing: false},
            {sno:31,name:'Abdullah Zulfiqar',gender:'Male',  editing: false},
            {sno:32,name:'Tom Campbell',gender:'Male',  editing: false},
            {sno:33,name:'Abdullah Zulfiqar',gender:'Male',  editing: false},
            {sno:34,name:'Tom Campbell',gender:'Male',  editing: false},
            {sno:35,name:'Abdullah Zulfiqar',gender:'Male',  editing: false},
            {sno:36,name:'Tom Campbell',gender:'Male',  editing: false},
            {sno:37,name:'Abdullah Zulfiqar',gender:'Male',  editing: false},
            {sno:38,name:'Tom Campbell',gender:'Male',  editing: false},
            {sno:39,name:'Abdullah Zulfiqar',gender:'Male',  editing: false},
            {sno:40,name:'Tom Campbell',gender:'Male',  editing: false},
//            {sno:21,name:'Abdullah Zulfiqar',gender:'Male'},
//            {sno:22,name:'Tom Campbell',gender:'Male'},            
//            {sno:23,name:'Abdullah Zulfiqar',gender:'Male'},
//            {sno:24,name:'Tom Campbell',gender:'Male'},
//            {sno:25,name:'Abdullah Zulfiqar',gender:'Male'},
//            {sno:26,name:'Tom Campbell',gender:'Male'},
//            {sno:27,name:'Abdullah Zulfiqar',gender:'Male'},
//            {sno:28,name:'Tom Campbell',gender:'Male'},
//            {sno:29,name:'Abdullah Zulfiqar',gender:'Male'},
//            {sno:30,name:'Tom Campbell',gender:'Male'},
//            {sno:31,name:'Abdullah Zulfiqar',gender:'Male'},
//            {sno:32,name:'Tom Campbell',gender:'Male'},
//            {sno:33,name:'Abdullah Zulfiqar',gender:'Male'},
//            {sno:34,name:'Tom Campbell',gender:'Male'},
//            {sno:35,name:'Abdullah Zulfiqar',gender:'Male'},
//            {sno:36,name:'Tom Campbell',gender:'Male'},
//            {sno:37,name:'Abdullah Zulfiqar',gender:'Male'},
//            {sno:38,name:'Tom Campbell',gender:'Male'},
//            {sno:39,name:'Abdullah Zulfiqar',gender:'Male'},
//            {sno:40,name:'Tom Campbell',gender:'Male'},
//            {sno:41,name:'Abdullah Zulfiqar',gender:'Male'},
//            {sno:42,name:'Tom Campbell',gender:'Male'},
//            {sno:43,name:'Abdullah Zulfiqar',gender:'Male'},
//            {sno:44,name:'Tom Campbell',gender:'Male'},
        ];
    
    // column to sort
    $scope.column = 'sno';
    
    // sort ordering (Ascending or Descending). Set true for desending
    $scope.reverse = false;   
    
    // called on header click
    $scope.sortColumn = function(col){
        $scope.column = col;
        if($scope.reverse){
            $scope.reverse = false;
            $scope.reverseclass = 'arrow-up';
        }else{
            $scope.reverse = true;
            $scope.reverseclass = 'arrow-down';
        }
    };
    
    // remove and change class
    $scope.sortClass = function(col){
        if($scope.column == col ){
            if($scope.reverse){
                return 'arrow-down';    
            }else{
                return 'arrow-up';
            }
        }else{
            return '';
        }
    }   
    

    $scope.editItem = function (person) {
    	person.editing = true;
    }

    $scope.doneEditing = function (person) {
    	person.editing = false;
        
    };
    
    $scope.currentPage = 1; 
    
    $scope.totalItems = $scope.persons.length;  
    $scope.numPerPage = 5;  
    $scope.paginate = function (value) {  
      var begin, end, index;  
      begin = ($scope.currentPage - 1) * $scope.numPerPage;  
      end = begin + $scope.numPerPage;  
      index = $scope.persons.indexOf(value);  
      return (begin <= index && index < end);  
    };  
    
    		this.tab = 1;
        
        this.selectTab = function (setTab){
        	this.tab = setTab;
        };
        
        this.isSelected = function(checkTab) {
        	return this.tab === checkTab;
        };

});